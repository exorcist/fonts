### Fonts.

## Info
Most fonts in this repository have a regular(unpatched) font variant and a patched(nerd fonts) variant.

* Disclaimer: Bitmap fonts cannot be patched because of the way they are made.

Piracy good, [DMCA](https://github.com/github/dmca) and
[RIAA](https://en.wikipedia.org/wiki/Recording_Industry_Association_of_America)(does not really have anything in common with the fonts but still fuck them) bad.

Fork this repository if you are willing to help keep these fonts free forever.

## Installation
If you do not want to clone 1GB of junk clone with:
```sh
git clone https://gitlab.com/exorcist365/fonts --depth=1 --branch=master
```
or if you want a sparse clone run:
```sh
git clone https://gitlab.com/exorcist365/fonts --depth=1 --filter=blob:none --sparse
```
Do **NOT** place the fonts folder directly in your home directory. It just clutters it up and it's pointless. `~/.local/share` works as well.

If you are just downloading a single font folder you may need to create ~/.local/share/fonts and then place the font family there.

Copy the fonts directory to `~/.local/share/` and run the following command:
```sh
fc-cache -fv ~/.local/share/fonts
```
